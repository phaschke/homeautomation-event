package com.peterchaschke.homeautomation.eventserver;

import java.util.ArrayList;

public class Event {
	
	private String controlName;
	private String event;
	private ArrayList<EventParameter> parameters;
	
	Event() {}
	
	Event(String controlName, String event, ArrayList<EventParameter> parameters) {
		this.setControlName(controlName);
		this.setEvent(event);
		this.setParameters(parameters);
	}

	public String getControlName() {
		return controlName;
	}

	public void setControlName(String controlName) {
		this.controlName = controlName;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public ArrayList<EventParameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<EventParameter> parameters) {
		this.parameters = parameters;
	}
	
}
