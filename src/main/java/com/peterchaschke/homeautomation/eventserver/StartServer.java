package com.peterchaschke.homeautomation.eventserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;



@Component
public class StartServer implements ApplicationListener<ApplicationReadyEvent> {
	
	@Value( "${mqtt.url}" )
	private String mqttUrl;
	
	@Value( "${mqtt.topic}" )
	private String mqttTopic;
	
	@Value("${core.event.url}")
	private String coreEventUrl;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		
		MQTTClient client = new MQTTClient(mqttUrl, mqttTopic, coreEventUrl);
    	client.subscribe();
		
	}

}
